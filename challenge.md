The next step in our hiring process is what we like to call: the Remote Operations Challenge!  It's a pretty easy test that I hope won't take too much time, but should give us some insight into how you approach operational tasks.

I'd like to apologize for the delay in sending you this challenge.  We usually try to send them out same-day.  Should you decide to get started on it this weekend, I'll be sure to check my email periodically over the weekend in case you run into any trouble as you get started.  Take as much time as you need.

I really appreciate you taking the time to do this as it serves as your "introduction" to the rest of the team: our team will look very closely at your submission and collectively weigh in with their opinions and recommendations, hopes, and fears. Because everybody has gone through a very similar process, this gives us a level playing field on which to evaluate all new candidates -- the better you do on this part of the process, the easier it is for me to sell you to the rest of the team.   Furthermore, by doing this test you help us train our interview process by adding another datapoint on which to evaluate future hires.

Basically, we put an absolute ton of energy into hiring, and once you come on board, it's the way we ensure that you stay surrounded by the best people we can possibly find. Thanks for your patience with the process.

So, without further ado, I'd like to present the challenge:

I've spun up four micro instances for you in AWS; we haven't done anything crazy with them-- they're bone-stock Ubuntu 14.04 AMIs.  Username is "ubuntu," your GitHub SSH key controls access.  If you don't have a good SSH key associated with your GitHub account, please send one my way and I'll be happy to get you squared away.

    54.202.23.208
    34.220.47.179
    34.220.190.66
    52.88.243.46

What I'd like you to do is turn them into a miniature environment.  I'd like to see:

- detailed notes of your work - challenges, design choices, etc. Please be descriptive, so that we get, not just the end result, but an idea of your thought process, reasons for the approaches you take, how you solve problems you encounter along the way, etc. 

- 2 web servers - one serves 'a', the other 'b' at index.html
- add a load balancer on the 3rd server to load balance between the webservers
- configure the load balancer
- any balancing scheme (round robin, random, load based, etc.)
- configure the load balancer to be "sticky" - the same host should hit the same webserver for repeat requests, only switching when a webserver goes down, and not switching back when the webserver goes back up
- pass the original requesting ip to the webservers
- make port range 60000-65000 on the load balancer all get fed to the web servers on port 80.
- add nagios to the 4th server and configure to monitor the servers and the load balancer.
- add a user ‘expensify’ to the boxes; grant me sudo access and install the attached public key as its authentication credential
- lock down the network:
	- allow only one server public ssh access
	- from that server, be able to access the others via ssh
	- block all other unused/unnecessary ports

Feel free to use whatever resources you have at your disposal, within reason; we do want to see *your* work, so "paying a consultant to do it for you" is out of bounds, but this isn't a cleanroom environment; we use many external resources day-to-day, and generally we don't find the idea of "do $task without any of the resources you'd normally use to do it" to be anything approaching a reasonable challenge.  Just document whatever external sources you use, and be sure to ask if you have any questions about any parts of the instructions.

Sincerely,
Sam Whitney