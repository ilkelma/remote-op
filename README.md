# introduction

Hi Sam and Expensify infrastructure team. I've opted to type up my notes as the README so they're visible on the gitlab repo page. This file should contain all the notes and explanations plus links to source material.

## first a note on hosts
I address this in other places but basically for this repo to work without any further tampering you would need to modify your hosts file wherever this was run to add the following:

```
54.202.23.208   load-balancer
34.220.47.179   backend-a
34.220.190.66   backend-b
52.88.243.46    bastion
```

This was a design decision I made based on the need for feedback while working on it. Ansible would print IP addresses to indicate which host a play was run on which would make the output harder to follow. If you changed the hosts file to IP addresses most things would work with some minor tweaks. This was a trade off between ease of development and ease of portability to be run anywhere with a minimum install of ansible. I choose the ease of development in this case because in the real world you would often have hostnames of some description (even if only internally) via DNS and also there was little need to be super portable.

## design plans - 📋🏗
Scoping the requirements comes up with a reasonable plan that I can work from. I'll use Nginx to load balance and function as the port 80 proxy for the web servers. I'll run a simple Golang app to demonstrate and debug which server is responding. It looks like I'll also need to pass some headers to the backends to get the real IP. I've used nagios before and built checks but never installed the system so I'll need to look into how that's done. I'll need to modify users a bit as well as set up some firewall rules.

Here's my checklist:

- [x] Set up firewall rules
- [x] Install necessary software packages (nginx, nagios, etc.)
- [x] Build tiny server app and run it
- [x] Configure nginx backends 
- [x] configure nginx load balancer
- [x] configure nagios and add appropriate checks
- [x] add expensify user

## humble beginnings
First things first I jumped into one of the server to make sure I had sudo and check a few things. Just checking into things like whether it uses `systemd` or `init` (scripts). See what tools are enabled and what quality of life things I should change (making sure vim is installed, etc.). I come from mainly a RHEL/CentOS world so some flavor things may require some adjustment/googling. I later learned in fact all about upstart scripts which are like halfway between bare init and systemd - interesting but I definitely prefer systemd.

One such example is on the firewall side. I know several of the tasks involve some firewall manipulation and on CentOS all but the recent releases use iptables but I believe Ubuntu uses `ufw`. That's actually good though as I plan to use ansible and there exists a [ufw module] to make that nicer. Iptables would need to be a template and that can get ugly fast. (Fun fact, it did get ugly! or at least interesting...)

Additionally I see it's init.d scripts and not systemd. That means I'll want to install monit to automatically reboot machines.

I also set up a few things client side to make my life easier, modifying my /etc/hosts file to use the same names as I did with the .hosts file for ansible. Sort of a local only DNS setup so I don't have to remember the IPs. I also modified my SSH config to automatically use the ubuntu user instead of my user.

## ansible all the things
For tasks like this (and most configuration management tasks) I would use Ansible to configure in a way that would be repeatable. I'm also going to make this a git repository and post it on gitlab (private repo).

So I set up ansible with a default ansible.cfg file and a .hosts file where I provided convenient groups to refer to hosts for variables and other things. I like to organize things in ansible along a few different lines. First I split out larger functionality into either tasks files or roles that I then include into the main `setup.yml` file. Generally if the functionality includes files or templates or is more generally reusable then it's usually a good fit for being a role but if it's a discrete sequence of tasks that doesn't need anything special then it's often better as a task file.

I also use group_vars when I can as it allows you to separate out config for each server or group of servers without having to include it in your tasks/roles.

## first things first = lock it down
If this were a real project it would have been locked down from the start via security groups in AWS and then only opened once everything is really done. That said, first task in my mind is to lock it down so we don't have any weird things going down.

First things I'll do is update `sshd_config` and then the ufw config. I'm going to create an ansible role for sshd_config as that'll need to be applied to all machines but be different. The ansible role will not be all encompassing (in the sense of having all options available) for the sake of time but hopefully by looking at the template in `roles/sshd/templates/sshd_config.j2` you can get a sense of how it would look if I accounted for every option. There's actually a repository called ansible galaxy that contains roles defined in that broad sense but I've not had good luck with those roles, there's always some quirk they don't account for. Additionally I figured pulling in one of those might not be in the spirit of this enterprise (checking my skills rather than my ability to `git clone`).

The next thing to do was lock down the firewall. I spent a great deal of time looking up how ufw works and how it can work with ansible. The vast majority was simple and I quickly had port 22 open on the bastion host and closed on all the others (exept for the bastion). Slightly trickier was modifying sshd and my client end to allow me to transparently tunnel through the bastion when I needed to ssh to machines behind the bastion. My ssh config looked like the following:
```
Host bastion
ProxyCommand none
User ubuntu

Host load-balancer
ProxyCommand ssh bastion -W %h:%p
User ubuntu

Host backend-*
ProxyCommand ssh bastion -W %h:%p
User ubuntu
```

I made extensive use of this digital ocean article while setting up the firewall as well: [digitalocean ufw guide]

## Nginx as a load balancer

This was as easy as I hoped it would be. Nginx was relatively straightforward to set up as a proxy and pass all the headers through. Load balancing is a simple group of upstreams and a setting for strategy. I used ip_hash in order to provide the sticky sessions behavior.

I also built a [small golang web server] to run behind the nginx backends. It's a tiny script but I wanted to demonstrate the use of X-Real-IP headers to pass on the actual client IP to the backend servers. Originally I was going to use flask but getting the tooling and dependencies set up would take a great deal of time so instead I used Gitlab CI to automatically build my tiny server and provide an endpoint to just download it and use a simple upstart script to daemonize it.

The request to forward ports 60000 - 65000 was an interesting one to tackle. At first I thought I could just tell nginx to listen on all those ports but that would have involved 5000 separate listen statements which could be done programmatically with ansible but that also sucks up 5000 file handles for the tcp sockets which is probably not a great idea. I then figured I could use ufw for this and sure enough this server fault answer [ufw as port forward] indicated it was possible. Sadly not through ansible's ufw module though. I had to synthesize that answer with the official docs ([ubuntu ufw framework manpage]) to make sure it wasn't breaking anything as well. Using thise technique means nginx only has to listen on 80.

I did consider setting up let's encrypt as a bonus to actually see SSL working but that would likely require real domain names which I didn't really want to purchase just for this exercise.

## Nagios

Wow, this one I thought I knew what I was doing but apparently I've only ever used icinga which isn't the same thing. I was familiar with the server checks and getting NRPE hooked up at least but the install process was rough. I was hoping for a simple package installation but unfortunately it needs to be built from source (or deal with a very old version). I essentially ansible-ized the [nagios core docs] and the [digitalocean nagios guide] in order to get it all set up. In the end I added some lightly modified checks but nothing super interesting. I added http checks but I didn't have time to add nrpe based checks for the backend. So if you look at the service panel in nagios the backend servers both show as critical but that's correct since the firewall rule only allows the load balancer to reach them on port 80.

Oh and the password for the instance is in the email reply I sent passing this on to Sam. For ansible purposes and not checking it into the code since this is a public repository I stored it as an environment variable. That'd be necessary to run these ansible playbooks.

## various troubles encountered

### open failed: administratively prohibited 

This one was fun. The error was not terribly helpful but thanks to stack overflow ([admin prohibited]) I quickly realized it was my proxy commands for ssh not being able to resolve the names I'd given the servers in my local /etc/hosts file. So I copied out those /etc/hosts definitions to all the servers and all was well.

### nrpe connection refused

This was one was a bit tricky though it was kind of a "duh" moment when I figured it out. running nrpe check from the nagios machine to any of the other servers was failing and I couldn't figure out why. The firewall rule seemed like it was working just fine. I checked the logs and they were also not clear - there were ufw messages but they had nothing to do with my checks (diff IPs). I ran a netstat and then it was obvious - I'd told nrpe to bind to 127.0.0.1 which meant it didn't bind to the correct interface so my checks were calling but no one was listening. I changed it to 0.0.0.0 to listen on all interfaces and it was all set.

## final notes

This took a bit more time than I thought initially particularly around nagios but it was still quite fun to do so thank you for the opportunity to do this. I hope to speak more in depth about my design decisions and techniques soon! Thank you for taking the time to look at this. If there's anything in this document that's unclear please do ask or if there's more you'd like me to add before reviewing please let me know.

[ufw module]: https://docs.ansible.com/ansible/latest/modules/ufw_module.html
[nginx as load balancer]: https://nginx.org/en/docs/http/load_balancing.html#nginx_load_balancing_with_ip_hash
[ufw as port forward]: https://serverfault.com/questions/238563/can-i-use-ufw-to-setup-a-port-forward
[ubuntu ufw framework manpage]: http://manpages.ubuntu.com/manpages/precise/en/man8/ufw-framework.8.html
[admin prohibited]: https://unix.stackexchange.com/questions/14160/ssh-tunneling-error-channel-1-open-failed-administratively-prohibited-open
[digitalocean ufw guide]: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-14-04
[digitalocean nagios guide]: https://www.digitalocean.com/community/tutorials/how-to-install-nagios-4-and-monitor-your-servers-on-ubuntu-14-04#monitor-a-host-with-nrpe
[nagios core docs]: https://support.nagios.com/kb/article/nagios-core-installing-nagios-core-from-source-96.html#Ubuntu
[small golang web server]: https://gitlab.com/ilkelma/tiny-server